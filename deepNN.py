import numpy as np
import scipy.io as sio


nw = 2
nq = 7
MatRxxtemp=sio.loadmat('MatRxxph_arP1.mat')
MatRxx=np.array(MatRxxtemp['MatRxxph_ar'])
n,N=np.shape(MatRxx)
print(f' features: {n},\n realisations: {N}')
MatRww_d = MatRxx[0:nw-1, 0:]
MatRrr_d = MatRxx[nw:nq-1, 0:]
MatRvv_d = MatRxx[nw+nq-1:, 0:]



